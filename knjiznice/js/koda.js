/* global L */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var wikiUrl = 'https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=jsonfm&formatversion=2&titles=';



// seznam z markerji na mapi
var markerji = [];

var poly = [];
var poly2 = [];

var mapa;
var obmocje;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
var UKC_LAT = 46.0548693;
var UKC_LNG = 14.5202926;

window.addEventListener('load', function () {
    var mapOptions = {
    center: [UKC_LAT, UKC_LNG],
    zoom: 12
    //maxZoom: 3
  };
  var laatlnng = [];
   mapa = new L.map('mapa_id', mapOptions);
   var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
   var popup = L.popup();
   pridobiPodatke(function (jsonRezultat) {
    izrisRezultatov(jsonRezultat);
    
  });
  
  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    laatlnng = latlng;
    
    /*popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);*/
  pridobiPodatke(function(jsonRezultat) {
    //console.log("yes");
     obarvajNajblizje(latlng, jsonRezultat);
     
  });
  //var z = mapa.getZoom();
    //mapa.zoomIn();
    //mapa.zoomOut();
    mapa.panBy([1,0]);
  }
  
  mapa.on('click', obKlikuNaMapo);
  /*setInterval(function(){
    
  }, 200);*/
  
  
  mapa.on('movestart',function(){
    var ll;
    //console.log("sure");
    if(laatlnng.length != 0){
      ll = laatlnng;
      pridobiPodatke(function(jsonRezultat){
        obarvajNajblizje(ll, jsonRezultat);
      });
    }
    
  });
  //console.log("neeeeeeeeeeeeeeeeeeeeeeeee");
  mapa.on('zoomstart',function(){
    var ll;
    //console.log("sure2");
    if(laatlnng.length != 0){
      ll = laatlnng;
      pridobiPodatke(function(jsonRezultat){
        obarvajNajblizje(ll, jsonRezultat);
      });
    }
    
  });
  
  


  
});

//vpis.changed(wikiGet());
/*setInterval(function(){
  var be = document.getElementById("diagnoza").value();
  console.log(be);
  console.log("wat");
}, 500);*/



function obarvajNajblizje(latLng, jsonRezultat){
  for (var i=0; i < poly.length; i++) {
        mapa.removeLayer(poly[i]);  
  }
  var l = latLng.toString().split("(");
  var ll = l.toString().split(")");
  var lll = ll.toString().split(",");
  var lat1 = parseFloat(lll[1]);
  var lon1 = parseFloat(lll[2]);
  //console.log("latLng "+lat1);
  
  //drawnItems.getLayers().filter(l=>l instanceof L.Marker).forEach(l=>map.remove(l));
  var znac = jsonRezultat.features;
  for(var i = 0; i < znac.length; i++){
    var jeObmocje = 
      typeof(znac[i].geometry.coordinates[0]) == "object";
    //console.log(jeObmocje);
    var latlngs = znac[i].geometry.coordinates;
    var lng2 = jeObmocje ? znac[i].geometry.coordinates[0][0][0] : 
      znac[i].geometry.coordinates[0];
    var lat2 = jeObmocje ? znac[i].geometry.coordinates[0][0][1] : 
      znac[i].geometry.coordinates[1];
    var razd = dist(lat1, lon1, lat2, lng2);
    for(var j = 0; j < znac[i].geometry.coordinates.length; j++){
     for(var k = 0; k < latlngs[j].length; k++){
       var first = latlngs[j][k][0];
       latlngs[j][k][0] = latlngs[j][k][1];
        latlngs[j][k][1] = first;
     }
     
     
     
   }
   //console.log("lat1 "+ lat1 + " lon1 "+ lon1 + " lat2 "+ lat2+" lng2 "+ lng2);
   //console.log("razd "+razd);
    var polygon;
    if(razd <= 0.01 && znac[i].geometry.type == "Polygon"){
      polygon = L.polygon(latlngs, {color: 'green'}).addTo(mapa);
      poly.push(polygon);
    }
    else if(znac[i].geometry.type == "Polygon"){
      polygon = L.polygon(latlngs, {color: 'blue'}).addTo(mapa);
      poly.push(polygon);
    }
    
  }
  
  
}

function pridobiPodatke(callback) {
  //if (typeof(vrstaInteresneTocke) != "string") return;

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        // nastavimo ustrezna polja (število najdenih zadetkov)
        
        // vrnemo rezultat
        //console.log("json je " + json.features);
        callback(json);
    }
  };
  xobj.send(null);
}

function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 'red' + '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>" + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}

function izrisRezultatov(jsonRezultat) {
  var znacilnosti = jsonRezultat.features;
  //console.log('znacilnosti '+ znacilnosti);
  var ad = 'addr:street';
  var hn = 'addr:housenumber';
  for (var i = 0; i < znacilnosti.length; i++) {
    var im = undefined;
    var addrs = undefined;
    var addrhn = undefined;
    var jeObmocje = false;
    if(znacilnosti[i] != undefined && znacilnosti[i].geometry != undefined){
      jeObmocje = 
      typeof(znacilnosti[i].geometry.coordinates[0]) == "object";
    }
    if(znacilnosti[i] != undefined && znacilnosti[i].properties != undefined){
      im = znacilnosti[i].properties.name;
      addrs = znacilnosti[i].properties[ad];
      addrhn = znacilnosti[i].properties[hn];
    }
    
    if(im == undefined) im = '';
    if(addrs == undefined) addrs = '';
    if(addrhn == undefined) addrhn = '';
    //if(i <=20)console.log("im je "+ im + " addrs je "+addrs + " hn je " + addrhn);
    var opis = im +'<br>' + addrs + ' ' + addrhn;
   //if(i==0) console.log(znacilnosti[i].properties[ad]);
   var latlngs = [];
   if(znacilnosti[i] != undefined && znacilnosti[i].geometry != undefined){
     latlngs = znacilnosti[i].geometry.coordinates;
   }
   
   //latlngs.setLatLngs(latlngs);
   //if(i==0) console.log(latlngs[0]);
   
    // pridobimo koordinate
    var lng = 0;
    var lat = 0;
    if(znacilnosti[i] != undefined && znacilnosti[i].geometry != undefined){
        lng = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][0] : 
      znacilnosti[i].geometry.coordinates[0];
        lat = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][1] : 
      znacilnosti[i].geometry.coordinates[1];
      if(lng != undefined && lat != undefined) dodajMarker(lat, lng, opis);
      
    }
    
   
      //console.log("yes + jeobmocje "+jeObmocje + "koordinata " +znacilnosti[i].geometry.coordinates[0][0][1]);
    
    if(znacilnosti[i] != undefined && znacilnosti[i].geometry != undefined){
   for(var j = 0; j < znacilnosti[i].geometry.coordinates.length; j++){
     for(var k = 0; k < latlngs[j].length; k++){
       var first = latlngs[j][k][0];
       latlngs[j][k][0] = latlngs[j][k][1];
        latlngs[j][k][1] = first;
     }
     
     
     
   }
    }
   //if(i==0) console.log(latlngs[0]);
   if(znacilnosti[i] != undefined && znacilnosti[i].geometry != undefined && znacilnosti[i].geometry.type == "Polygon"){
     var polygon = L.polygon(latlngs, {color: 'red'}).addTo(mapa);
      poly.push(polygon);
   }
   
  }
}

function prikaziOznako(lng, lat) {
    return true;
}

function dist(lat1, lon1, lat2, lon2){
  return Math.sqrt((lat1-lat2)*(lat1-lat2)+(lon1-lon2)*(lon1-lon2));
}

